[![](https://images.microbadger.com/badges/image/colisweb/semantic-release.svg)](https://microbadger.com/images/colisweb/semantic-release "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/colisweb/semantic-release.svg)](https://microbadger.com/images/colisweb/semantic-release "Get your own version badge on microbadger.com")

Docker image to use semantic-release in our CI. When a `feat:` or `fix:` commit is encountered:
- creates a tag (which will trigger another part of the CI)
- creates a gitlab release
- notify us on chat

See <https://semantic-release.gitbook.io/semantic-release/>


